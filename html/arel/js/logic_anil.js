function trackingTypeToAnalyticsType(trackingState)
{
  var result = 'UNKOWN['+trackingState+']';
  switch (trackingState)
  {
    case arel.Tracking.MARKERLESS_3D:
      result = arel.Plugin.Analytics.Category.TYPE_ML3D;
      break;
    case arel.Tracking.MARKERLESS_2D:
      result = arel.Plugin.Analytics.Category.TYPE_ML2D;
      break;
    case arel.Tracking.BARCODE_QR:
      result  = arel.Plugin.Analytics.Category.TYPE_CODE;
      break;
    case arel.Tracking.MARKER:
      result = arel.Plugin.Analytics.Category.TYPE_MARKER;
      break;
  }
  return result;
}
function _GET(key)
{
  var getParameterString = window.location.search.substring(1);
  var getParameters = getParameterString.split('&');
  for (var i in getParameters)
  {
    var keyValuePair = getParameters[i].split('=');
    if (keyValuePair[0] == key)
    {
      return keyValuePair[1];
    }
  }
  return undefined;
}
isAndroid = _GET('device') == 'android';
function openYouTubeVideo(youTubeLink,name,webView)
{
  var isUIWebView = false;
  if(!webView)
  {
	isUIWebView = /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent);
  }
	
  arel.Media.openWebsite(youTubeLink, !isUIWebView);
  googleAnalytics.logYouTubeVideo(encodeURIComponent(name));
}
ACCOUNT_ID = google_analytics_id;
googleAnalytics =  new arel.Plugin.Analytics (ACCOUNT_ID, arel.Plugin.Analytics.EventSampling.ONCE, '');
arel.sceneReady(function()
{
  arel.Events.setListener(arel.Scene, function(type, param){trackingHandler(type, param);});
  arel.Scene.getTrackingValues(function(param){trackingHandler('dummy', param);});
  var sceneObjects = arel.Scene.getObjects();
  for (var i in sceneObjects)
  {
    arel.Events.setListener(sceneObjects[i], handleObjectCallbacks);
  }
  
  googleAnalytics.logSceneReady();
});

function trackingHandler(type, param)
{
  try
  {
    if (!type || !param[0]) return;
    var cosId = param[0].getCoordinateSystemID();
    var cosName = param[0].getCoordinateSystemName();
    var state = param[0].getState();
    var trackingMethod  = param[0].getType();
    var gaTrackingMethod = trackingTypeToAnalyticsType(trackingMethod);
    googleAnalytics.logTrackingEvent(gaTrackingMethod, arel.Plugin.Analytics.Action.STATE_TRACKING, cosId, cosName);
  }
  catch(e)
  {
    alert(e.message);
  }
}
function handleObjectCallbacks(obj, type, params)
{
  try
  {
    var objectId = obj.getID();
    if (arel.Events.Object.ONTOUCHSTARTED == type)
    {
      googleAnalytics.logUIInteraction(arel.Plugin.Analytics.Action.TOUCHSTARTED, objectId)
    }
    else if (arel.Events.Object.TOUCHENDED == type)
    {
      googleAnalytics.logUIInteraction(arel.Plugin.Analytics.Action.TOUCHENDED, objectId)
    }
	
    /* handle click object events */
    if ('4bf2c9750fcf74d06e3fb7106e2ba853' === objectId
    && arel.Events.Object.ONTOUCHSTARTED ===  type)
    {
      arel.Media.openWebsite('https://www.facebook.com/plugins/like.php?href=https://www.facebook.com/DBDCdance', false);;
      arel.Events.setListener('https://www.facebook.com/plugins/like.php?href=https://www.facebook.com/DBDCdance', handleMediaCallbacks);;
      googleAnalytics.logWebsite('https%3A%2F%2Fwww.facebook.com%2FDBDCdance');
    }

    if ('37f264efedae71c492ad46068f4fb9c9' === objectId
    && arel.Events.Object.ONTOUCHSTARTED ===  type)
    {
      openYouTubeVideo('http://www.youtube.com/watch?v=yPXqyxwyhLg','Opa Gangnam Style- DBDC', false);;
      arel.Events.setListener('http://www.youtube.com/watch?v=yPXqyxwyhLg', handleMediaCallbacks);;
    }
    
	/* handle click object events */
    if ('2b8665a552d2734e290329ee7cda41fc' === objectId
    && arel.Events.Object.ONTOUCHSTARTED ===  type)
    {
      arel.Media.openWebsite('http://www.dbdcdance.com', false);;
      arel.Events.setListener('http://www.dbdcdance.com', handleMediaCallbacks);;
      googleAnalytics.logWebsite('http%3A%2F%2Fwww.dbdcdance.com');
    }
	
	if ('763a5c3b85ef67e28fe855dbd40c5cc5' === objectId
    && arel.Events.Object.ONTOUCHSTARTED ===  type)
    {
      arel.Media.createCalendarEvent(new Date(2013,8,15,20, 00),new Date(2013,8,15,23, 00), 'Bollywood Dance Night', 'DBDC performs', 'Tampere');;
      arel.Debug.log('Triggered calendar event Bollywood Dance Night');;
    }
	
	 if ('6ed41e608f829d7ffb33bf41781b4cf5' === objectId
    && arel.Events.Object.ONTOUCHSTARTED ===  type)
    {
      arel.Media.openWebsite('https://www.facebook.com/dialog/pay?app_id=458358780877780&
                                    redirect_uri=https://mighty-lowlands-6381.herokuapp.com&
                                    action=buy_item&
                                    order_info=SOME_DEV_JSON_ORDER_INFO&
                                    dev_purchase_params={"oscif":true}', false);;
      arel.Events.setListener('https://www.facebook.com/dialog/pay?app_id=458358780877780&
                                    redirect_uri=https://mighty-lowlands-6381.herokuapp.com&
                                    action=buy_item&
                                    order_info=SOME_DEV_JSON_ORDER_INFO&
                                    dev_purchase_params={"oscif":true}', handleMediaCallbacks);;
      googleAnalytics.logWebsite('https%3A%2F%2Fwww.facebook.com%2FDBDCdance');
    }
    
  }
  catch(e)
  {
    alert(e.message);
  }
}
function handleMediaCallbacks(eventType, mediaURL)
{
  try
  {
    arel.Events.removeListener(mediaURL);
  }
  catch(e)
  {
    alert(e.message);
  }
}
